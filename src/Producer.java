import java.util.Random;

public class Producer extends Thread{

    Container container = null;

    public Producer(Container container) {
        this.container = container;
    }

    public void produce() {
        Random random = new Random();
        int drawn = 4;
        drawn = random.nextInt(3);
        if (drawn == 0) {
            this.container.produceFirstProduct();
        } else if (drawn == 1) {
            this.container.produceSecondProduct();
        } else {
            this.container.produceThirdProduct();
        }
    }


    @Override
    public void run() {
        while (true) {
            produce();
            Random random = new Random();
            int sleepTime = random.nextInt(2000) + 400;
            try {
                sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
