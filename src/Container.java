public class Container {
    public int firstProduct = 0;
    public int secondProduct = 0;
    public int thirdProduct = 0;
    public int capacity = 0;

    public Container(int capacity) {
        this.capacity = capacity;
    }

    public synchronized void buyFirstProduct(int amount) {
        if (this.firstProduct < amount) {
            System.out.println("Jest za mało pierwszego produktu");
        } else {
            this.firstProduct -= amount;
            System.out.println("Kupiono " + amount + "sztuk pierwszego produktu");
        }
    }

    public synchronized void buySecondProduct(int amount) {
        if (this.secondProduct < amount) {
            System.out.println("Jest za mało drugiego produktu");
        } else {
            this.secondProduct -= amount;
            System.out.println("Kupiono " + amount + "sztuk drugiego produktu");
        }
    }

    public synchronized void buyThirdProduct(int amount) {
        if (this.thirdProduct < amount) {
            System.out.println("Jest za mało trzeciego produktu");
        } else {
            this.thirdProduct -= amount;
            System.out.println("Kupiono " + amount + " sztuk tzeciego produktu");
        }
    }

    public synchronized void produceThirdProduct() {
        if (this.thirdProduct < this.capacity) {
            this.thirdProduct++;
            System.out.println("Wyprodukowano trzeci produkt");
        } else {
            System.out.println("Już nie ma miejsca na trzeci produkt!");
        }
    }

    public synchronized void produceSecondProduct() {
        if (this.secondProduct < this.capacity) {
            this.secondProduct++;
            System.out.println("Wyprodukowano drugi produkt");
        } else {
            System.out.println("Już nie ma miejsca na drugi produkt!");
        }
    }

    public synchronized void produceFirstProduct() {
        if (this.firstProduct < this.capacity) {
            this.firstProduct++;
            System.out.println("Wyprodukowano pierwszy produkt");
        } else {
            System.out.println("Już nie ma miejsca na pierwszy produkt!");
        }
    }

}
