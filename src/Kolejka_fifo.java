
// implementacja kolejki fifo
class Kolejka_fifo
{
    Element pocz = null , koniec = null ;
    public synchronized void dodaj ( Integer s )
    { // to juz sekcja krytyczna
        Element e = new Element () ;
        e . dana = s ;
// dodanie elementu do kolejki
        if ( koniec == null ) // kolejka pusta
            pocz = e ;
        else {
            e.nast = pocz;
            pocz = e;
        }
        e.nast = null ;
// poinformuj czekajacy watek
        notify () ;
    }
    public synchronized Integer odczytaj ()
    {
        try
        { // warunek zmiennej warunkowej
            while ( pocz == null )
                wait () ;
        }
        catch ( InterruptedException e )
        {
            return null ;
        }
// element do zwrotu
        Element ret = pocz ;
// usuniecie z kolejki
        pocz = pocz.nast ;
// kolejka pusta
        if ( pocz == null )
            koniec = null ;
        return ret.dana ;
    }
}