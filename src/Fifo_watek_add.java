import java.util.Random;

class Fifo_watek_add extends Thread {
    Kolejka_fifo f;

    // konstruktor
    Fifo_watek_add(Kolejka_fifo _f) {
        super();
        f = _f;
    }

    // funkcja watku
    public void run() {
        Random random = new Random();
        while(true) {
            Integer rndNumber = random.nextInt(100);
            f.dodaj(rndNumber);
            System.out.println(" Watek dodaje " + rndNumber);
            int sleepTime = random.nextInt(2000) + 1000;
            try {
                sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}