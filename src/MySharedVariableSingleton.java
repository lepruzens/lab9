public  class  MySharedVariableSingleton {
    private  static  MySharedVariableSingleton  instance = null;

    public  static  MySharedVariableSingleton  getInstance ()
    {
        if(instance ==null)
            instance = new  MySharedVariableSingleton ();
        return  instance;
    }
}