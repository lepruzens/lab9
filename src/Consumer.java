import java.util.Random;

public class Consumer extends Thread{
    Container container = null;

    public Consumer(Container container) {
        this.container = container;
    }

    public void consume() {
        Random random = new Random();
        int amount;
        int type;
        type = random.nextInt(3);
        amount = random.nextInt(10) + 1;

        if (type == 0) {
            this.container.buyFirstProduct(amount);
        } else if (type == 1) {
            this.container.buySecondProduct(amount);
        } else {
            this.container.buyThirdProduct(amount);
        }
    }

    public void run() {
        while (true) {
            this.consume();
            Random random = new Random();
            int sleepTime = random.nextInt(3000)+800;
            try {
                sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}


