public  class  HelloThread  extends  Thread {
    MySharedVariable  mySharedVariable = null;
    public  HelloThread(MySharedVariable  mySharedVariable) {
        this.mySharedVariable = mySharedVariable;
    }
    public  void  run() {
        mySharedVariable.increment ();
        System.out.println (" Wartosc  licznik = " +mySharedVariable.getCounter ());
    }
}