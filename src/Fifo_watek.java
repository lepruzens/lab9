import java.util.Random;

class Fifo_watek extends Thread {
    Kolejka_fifo f;

    // konstruktor
    Fifo_watek(Kolejka_fifo _f) {
        super();
        f = _f;
    }

    // funkcja watku
    public void run() {
        Random random = new Random();
        while (true) {
            System.out.println(" Watek odczytal " + f.odczytaj());
            int sleepTime = random.nextInt(2000) + 2000;
            try {
                sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}